﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using OAuth2WebApiSample.Formats;
using OAuth2WebApiSample.OAuth.Providers;
using System;
using System.Web.Http;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using Microsoft.IdentityModel.Tokens;

[assembly: OwinStartup(typeof(OAuth2WebApiSample.Startup))]
namespace OAuth2WebApiSample
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);

            var httpConfig = new HttpConfiguration();
            WebApiConfig.Register(httpConfig);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(httpConfig);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthAuthorizationServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                //Provider = new SimpleAuthorizationServerProvider(),
                Provider = new JwtAuthorizationServerProvider(),
                // Specify just for JwtAuthorizationServerProvider()
                AccessTokenFormat = new CustomAsymmetricJwtFormat(ConfigurationManager.AppSettings["AuthServerIssuer"])
            };

            // Authorization Server Token Generation
            app.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions);

            // Resource Server Authentication settings for Authorization Server using SimpleAuthorizationServerProvider
            //app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            // Resource Server Authentication settings for Authorization Server using JwtAuthorizationServerProvider
            var issuer = ConfigurationManager.AppSettings["ResourceServerIssuer"];
            var audience = ConfigurationManager.AppSettings["ResourceServerAudience"];

            var certificate = new X509Certificate2(ConfigurationManager.AppSettings["ResourceServerJwtSecret"]);

            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { audience },
                    IssuerSecurityKeyProviders = new IIssuerSecurityKeyProvider[]
                    {
                        //new SymmetricKeyIssuerSecurityKeyProvider(issuer, secret)
                        new X509CertificateSecurityKeyProvider(issuer, certificate)
                    },
                    // Just needed for X509CertificateSecurityKeyProvider:
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKeyResolver = (a, b, c, d) => new[] { new X509SecurityKey(certificate) },
                        ValidAudience = audience,
                        ValidIssuer = issuer
                    }
                });
        }
    }
}