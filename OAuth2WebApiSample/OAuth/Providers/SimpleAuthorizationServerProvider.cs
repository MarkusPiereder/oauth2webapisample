﻿using Microsoft.Owin.Security.OAuth;
using OAuth2WebApiSample.Repositories;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OAuth2WebApiSample.OAuth.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (AuthRepository _repo = new AuthRepository())
            {
                var user = await _repo.FindUser(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));

            if (context.Scope != null && context.Scope.Contains("test"))
                identity.AddClaim(new Claim(ClaimTypes.Role, "testrolle1"));
            else
                identity.AddClaim(new Claim(ClaimTypes.Role, "user"));

            context.Validated(identity);
        }
    }
}